using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NUnit.Framework;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Shouldly;

namespace Otus.Teaching.PromoCodeFactory.UnitTest
{
    public class Tests
    {
        private IHost host;
        private HttpClient client;
        private EmployeeShortRequest newUser;

        [OneTimeSetUp]
        public void Setup()
        {
            host = CreateTestHost();
            host.StartAsync().Wait();
            var testServer = host.Services.GetRequiredService<IServer>().ShouldBeOfType<TestServer>();
            client = testServer.CreateClient();
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            host.StopAsync().Wait();
            client.Dispose();
            host.Dispose();
        }

        /// <summary>
        /// "/api/v1/Roles"
        /// </summary>
        [Test]
        public async Task TestRolesList()
        {
            var result = await client.GetAsync("/api/v1/Roles");
            result.StatusCode.ShouldBe(HttpStatusCode.OK);
            var res = await result.Content.ReadAsStringAsync();
            var list = JsonConvert.DeserializeObject<List<Role>>(res);
            list.Count.ShouldBe(FakeDataFactory.Roles.Count());

            foreach (var role in FakeDataFactory.Roles)
            {
                list.Exists(x => x.Id == role.Id).ShouldBe(true);
            }
        }


        [Test, Order(1)]
        public async Task TestAddEmloyee()
        {
            newUser = new EmployeeShortRequest()
            {
                Email = "test@mail.ru",
                AppliedPromocodesCount = 5,
                FirstName = "Иван",
                LastName = "Иванов",
                Id = Guid.NewGuid()
            };

            StringContent content = new StringContent(JsonConvert.SerializeObject(newUser));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var result = await client.PostAsync("/api/v1/Employees", content);
            result.StatusCode.ShouldBe(HttpStatusCode.Created);

            result = await client.GetAsync($"/api/v1/Employees/{newUser.Id}");
            result.StatusCode.ShouldBe(HttpStatusCode.OK);

            var res = await result.Content.ReadAsStringAsync();
            var user = JsonConvert.DeserializeObject<EmployeeResponse>(res);
            user.Id.ShouldBe(newUser.Id);
            user.Email.ShouldBe(newUser.Email);
            user.FullName.ShouldBe($"{newUser.FirstName} {newUser.LastName}");
            user.AppliedPromocodesCount.ShouldBe(newUser.AppliedPromocodesCount);
            user.Roles.Count().ShouldBe(0);
        }

        [Test, Order(2)]
        public async Task TestSetRoleEmloyee()
        {
            List<string> newRole = FakeDataFactory.Roles.Select(x => x.Name).ToList();

            var content = new StringContent(JsonConvert.SerializeObject(newRole));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var result = await client.PutAsync($"/api/v1/Employees/{newUser.Id}/Role", content);
            result.StatusCode.ShouldBe(HttpStatusCode.OK);

            result = await client.GetAsync($"/api/v1/Employees/{newUser.Id}");
            result.StatusCode.ShouldBe(HttpStatusCode.OK);

            var res = await result.Content.ReadAsStringAsync();
            var user = JsonConvert.DeserializeObject<EmployeeResponse>(res);
            user.Roles.Count().ShouldBe(newRole.Count);

            foreach (var role in newRole)
            {
                user.Roles.Exists(x => x.Name == role).ShouldBe(true);
            }
        }


        [Test, Order(3)]
        public async Task TestChangeEmloyee()
        {
            newUser.Email = "test1@mail.ru";
            newUser.AppliedPromocodesCount = 2;
            newUser.FirstName = "Иваныч";
            newUser.LastName = "Васильев";

            StringContent content = new StringContent(JsonConvert.SerializeObject(newUser));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var result = await client.PostAsync("/api/v1/Employees", content);
            result.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
            
            result = await client.PutAsync("/api/v1/Employees", content);
            result.StatusCode.ShouldBe(HttpStatusCode.OK);

            result = await client.GetAsync($"/api/v1/Employees/{newUser.Id}");
            result.StatusCode.ShouldBe(HttpStatusCode.OK);

            var res = await result.Content.ReadAsStringAsync();
            var user = JsonConvert.DeserializeObject<EmployeeResponse>(res);
            user.Id.ShouldBe(newUser.Id);
            user.Email.ShouldBe(newUser.Email);
            user.FullName.ShouldBe($"{newUser.FirstName} {newUser.LastName}");
            user.AppliedPromocodesCount.ShouldBe(newUser.AppliedPromocodesCount);
            user.Roles.Count().ShouldBe(2);
        }
        
        [Test, Order(4)]
        public async Task TestDeleteEmloyee()
        {
            var s = $"/api/v1/Employees/{Guid.NewGuid().ToString()}";
            var result = await client.DeleteAsync(s);
            result.StatusCode.ShouldBe(HttpStatusCode.NotFound);

            result = await client.DeleteAsync($"/api/v1/Employees/{newUser.Id}");
            result.StatusCode.ShouldBe(HttpStatusCode.OK);
            
            result = await client.GetAsync($"/api/v1/Employees/{newUser.Id}");
            result.StatusCode.ShouldBe(HttpStatusCode.NotFound);
        }


        private IHost CreateTestHost() =>
            Host.CreateDefaultBuilder()
                .ConfigureWebHostDefaults(webBuilder =>
                    webBuilder.UseStartup<Startup>()
                        .UseTestServer())
                .ConfigureServices(services =>
                    services
                        .AddLogging(logging =>
                            logging.ClearProviders().AddConsole()))
                .Build();
    }
}