﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(string))]
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description,
                    Id = x.Id
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Удаляет сотрудника по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(string))]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteEmployeeByIdAsync(Guid id)
        {
            bool result;
            try
            {
                result = await _employeeRepository.DeleteAsync(id);
            }
            catch (KeyNotFoundException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }

            return result ? StatusCode(StatusCodes.Status200OK) : StatusCode(StatusCodes.Status500InternalServerError);
        }

        /// <summary>
        /// Создание сотрудника 
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(string))]
        [HttpPost()]
        public async Task<ActionResult> InsertEmployee(EmployeeShortRequest employee)
        {
            try
            {
                if (employee == null) throw new ArgumentException("employee is empty");
                var newEmployee = NewEmployee(employee);
                newEmployee.Roles = new List<Role>();
                await _employeeRepository.InsertAsync(newEmployee);
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status400BadRequest, e.Message);
            }

            return StatusCode(StatusCodes.Status201Created);
        }

        /// <summary>
        /// изменение сотрудника 
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(string))]
        [HttpPut()]
        public async Task<ActionResult> UpdateEmployee(EmployeeShortRequest employee)
        {
            try
            {
                if (employee == null) throw new ArgumentException("empty employee");
                Employee newEmployee = NewEmployee(employee);
                var oldEmployee = await _employeeRepository.GetByIdAsync(employee.Id);
                if (oldEmployee == null) throw new KeyNotFoundException("employee not found");
                newEmployee.Roles = oldEmployee.Roles ?? new List<Role>();
                await _employeeRepository.UpdateAsync(newEmployee);
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status400BadRequest, e.Message);
            }
            catch (KeyNotFoundException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }

            return Ok();
        }


        /// <summary>
        /// Назначает роли сотруднику по id названиям ролей
        /// </summary>
        /// <param name="id"></param>
        /// <param name="listRoles"></param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(string))]
        [HttpPut("{id}/Role")]
        public async Task<ActionResult> SetEmployeeRole(Guid id, List<string> listRoles)
        {
            try
            {
                var employee = await _employeeRepository.GetByIdAsync(id);
                if (employee == null) throw new KeyNotFoundException("employee not found");
                var dictRoles = await _roleRepository.GetAllAsync();
                List<Role> newRoles = new List<Role>();
                foreach (var role in listRoles)
                {
                    var newRole = dictRoles.Select(x => x).Where(x => x.Name == role).ToList();
                    if (!newRole.Any())
                        return StatusCode(StatusCodes.Status400BadRequest, $" role {role} not found");
                    newRoles.AddRange(newRole);
                }

                employee.Roles = newRoles;
                await _employeeRepository.UpdateAsync(employee);
            }
            catch (ArgumentException e)
            {
                return StatusCode(StatusCodes.Status400BadRequest, e.Message);
            }
            catch (KeyNotFoundException e)
            {
                return StatusCode(StatusCodes.Status404NotFound, e.Message);
            }

            return Ok();
        }


        private Employee NewEmployee(EmployeeShortRequest employee)
        {
            return new Employee()
            {
                Id = employee.Id,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Email = employee.Email,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };
        }
    }
}