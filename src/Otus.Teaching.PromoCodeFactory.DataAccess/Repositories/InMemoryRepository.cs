﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IList<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task InsertAsync(T entity)
        {
            if (entity == null) throw new ArgumentException("entity is null");
            T item = GetByIdAsync(entity.Id).Result;
            if (item != null) throw new ArgumentException("entity already is exist");
            Data.Add(entity);
            return Task.CompletedTask;
        }

        public Task UpdateAsync(T entity)
        {
            if (entity == null) throw new ArgumentException("entity is null");
            T item = GetByIdAsync(entity.Id).Result;
            if (item == null) throw new KeyNotFoundException("entity not exist");
            Data[Data.IndexOf(item)] = entity;
            return Task.CompletedTask;
        }

        public Task<bool> DeleteAsync(Guid id)
        {
            var item = Data.FirstOrDefault(x => x.Id == id);
            if (item == null) throw new KeyNotFoundException("id not exist");
            return Task.FromResult(Data.Remove(item));
        }
    }
}